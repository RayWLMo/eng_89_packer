# Packer
## Installing Packer
- `curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -`
- `sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"`
- `sudo apt-get update -y`
- `sudo apt-get install packer -y`

## Making the Directory and Packer config file
- `mkdir packer_task`
- `cd packer_task`
- `sudo nano aws-ubuntu.pkr.hcl`

## Configuring the AMI
```
{
  "variables": {
    "instance_size": "t2.micro",
    "ami_name": "eng89_raymond_ami_packer",
    # Choosing which AMI to copy
    "base_ami": "ami-0a6e6cb27b2ee4a95",
    "ssh_username": "ubuntu",
    # Specifying which VPC and subnet to use
    "vpc_id": "vpc-07e47e9d90d2076da",
    "subnet_id": "subnet-0429d69d55dfad9d2"
  },
  "builders": [
  {
    "type": "amazon-ebs",
    "region": "eu-west-1",
    "source_ami": "{{user `base_ami`}}",
    "instance_type": "{{user `instance_size`}}",
    "ssh_username": "{{user `ssh_username`}}",
    "access_key": "X",
    "secret_key": "X",
    "ami_name": "{{user `ami_name`}}",
    "ssh_pty" : "true",
    "vpc_id": "{{user `vpc_id`}}",
    "subnet_id": "{{user `subnet_id`}}",
    "tags": {
    "Name": "App Name",
    "BuiltBy": "Packer"
    }
  }
]
}
```
## AWS Authentication
```bash
export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
```
## Initialise Packer Configuration
- `packer init .`

## Format and validate Packer template
- `packer fmt .`
- `packer validate .`

## Build Packer AMI
- Running this builder will create an EC2 instance and create an AMI from it before terminating the instance
- `packer build aws-ubuntu.pkr.hcl`
- Once the builder is finished, it should appear in the AWS [AMI page](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Images:sort=name) 